import time, threading

class Tamagotchi:
    def __init__(self):
        self._hunger = 50
        self._fullness = 50
        self._tiredness = 0
        self._happiness = 10
        self.status = {
            'mood': 'happy',
            'health': 'alive'
        }

    def checkValue(self,value):
        if value > 100:
            return 100
        elif value < 0:
            return 0
        else:
            return value


    def set_hunger(self, value):
        self._hunger = self.checkValue(value)

    def get_hunger(self):
        return self._hunger

    def set_fullness(self,value):
        self._fullness = self.checkValue(value)

    def get_fullness(self):
        return self._fullness

    def set_tiredness(self,value):
        self._tiredness = self.checkValue(value)

    def get_tiredness(self):
        return self._tiredness

    def set_happiness(self,value):
        self._happiness = self.checkValue(value)

    def get_happiness(self):
        return self._happiness

    hunger = property(get_hunger, set_hunger)
    fullness = property(get_fullness,set_fullness)
    tiredness = property(get_tiredness, set_tiredness)
    happiness = property(get_happiness,set_happiness)


    def feed(self):
        self.hunger -= 10
        self.fullness += 10
        if self.hunger < 70:
            self.status['mood'] = "happy"

    def play(self):
        self.happiness += 10
        self.tiredness += 10

    def bed(self, time):
        self.tiredness -= time * 10
        time.sleep(time)

    def poop(self):
        self.fullness -= 10

    def timepassed(self, timepassed):
        self.tiredness += timepassed / 1
        self.hunger += timepassed / 1
        self.happiness -= timepassed / 1
        if self.hunger >= 70 and self.status['mood'] == 'happy':
            self.status['mood'] = 'hungry'
            print('\nYour Tamagotchi is hungry')
        if self.hunger >= 100:
            self.status['health'] = 'dead'

    def start(self):
        time_interval = 1
        th = threading.Thread(target=self.timeflying, kwargs={'time_interval': time_interval})
        th.start()

    def timeflying(self,time_interval):
        while True:
            time.sleep(time_interval)
            self.timepassed(time_interval)


    def inspect(self):
        print("I am", self.status['health'])
        print("I am", self.status['mood'])
        print("Tiredness", int(self.tiredness))
        print("hunger", int(self.hunger))
        print("Happiness", int(self.happiness))
        print("Fullness", int(self.fullness))

if __name__ == "__main__":
    t = Tamagotchi()
    t.start()
    action_list = ['feed','play','bed','poop','inspect']
    action_dict = {'feed': t.feed,'play':t.play,'poop':t.poop,'inspect':t.inspect}
    while True:
        action = input('What do you want to do: feed, play, bed, poop, inspect?: ')
        if action in action_list:
            if action == "bed":
                time = input("How long would you like to sleep for? (in seconds)")
                t.bed(time)
            else:
                action_dict[action]()
        if t.status['health'] == 'dead':
            break
    print("You let your tamagotchi die, shame on you. GAME OVER!!!")
