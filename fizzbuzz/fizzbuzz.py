
def fizzBuzz(number=None):
    if not isinstance(number, int):
        return None
    msg = ''
    if number % 3 == 0:
        msg += 'Fizz'
    if number % 5 == 0:
        msg += 'Buzz'
    if number % 7 == 0:
        msg += 'Pop'
    if len(msg) == 0:
        return number
    else:
        return msg
    #      number
    # if number % 3 and number % 5 and number % 7:
    #     return


def test():
    print ('testing the pure number return:')
