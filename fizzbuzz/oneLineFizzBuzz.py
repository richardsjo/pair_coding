print("\n".join("FIZZ"*(i%3 == 0) + "BUZZ"*(i%5 == 0) + "POP"*(i%7 == 0) or str(i) for i in range(1, 101)))
