import unittest
from fizzbuzz import fizzBuzz


class FizzBuzzTest(unittest.TestCase):
    def setup(self):
        pass
    def testPureNumberReturn(self):
        result1 = fizzBuzz(2)
        result2 = fizzBuzz('3')
        result3 = fizzBuzz()
        result4 = fizzBuzz(1.1)
        self.assertEqual(result1, 2)
        self.assertEqual(result2, None)
        self.assertEqual(result3, None)
        self.assertEqual(result4, None)

    def testFizzBuzz(self):
        three_factor = fizzBuzz(3)
        normal_number = fizzBuzz(2)
        five_factor = fizzBuzz(5)
        seven_factor = fizzBuzz(7)
        factor_15 = fizzBuzz(15)
        factor_21 = fizzBuzz(21)
        factor_35 = fizzBuzz(35)
        factor_105 = fizzBuzz(105)
        self.assertEqual(three_factor,"Fizz")
        self.assertEqual(normal_number,2)
        self.assertEqual(five_factor,"Buzz")
        self.assertEqual(seven_factor,"Pop")
        self.assertEqual(factor_15,"FizzBuzz")
        self.assertEqual(factor_21,"FizzPop")
        self.assertEqual(factor_35,"BuzzPop")
        self.assertEqual(factor_105,"FizzBuzzPop")



if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(FizzBuzzTest))
    unittest.TextTestRunner(verbosity=2).run(suite)
